# 读取Excel文件
import openpyxl

class ExcelReader:
    # 构造函数
    def __init__(self, path:str, sheet:str, infos, flag:bool=True):
        self.path = path
        self.sheet = sheet
        self.infos = infos # 如果格式需要dict类型，需要提供key,infos就是提供的key
        self.flag = flag # 如果Flag是True,表明有表头，意味着数据从第二行开始，如果Flag是False,没有表头

        # 保存Excel数据的集合
        self.student01 = []  # [[],[],[],....]
        self.student02 = []  # [{},{},{},{},.....]

        # 自动读取
        self.read_file()

    def read_file(self):
        # 实例化一个
        workbook = openpyxl.load_workbook(self.path)
        # 定义一个sheet
        sheet = workbook[self.sheet]
        # 开始读取
        for index, row in enumerate(sheet.rows): # sheet.rows()-- 返回这个表格有多少行
            # 判断有没有表头
            if self.flag and index == 0:
                continue

            # 定义一个集合存储
            one_row_list = [] # 为了存储[[],[],[],....]
            one_row_dict = {}.fromkeys(self.infos) # 为了存储[{},{},{},{},.....]


            # 开始遍历一行的数据
            for col_index, col_value in enumerate(row):
                # ===把每一个单元格附加到one_row_list中
                one_row_list.append(col_value.value)
                # ===准备字典
                one_row_dict[self.infos[col_index]] = col_value.value

            # 附加到第一种类型的集合中--[[],[],[],....]
            self.student01.append(one_row_list)
            # 附加到第二种类型的集合中--[{},{},{},{},.....]
            self.student02.append(one_row_dict)
