"""
  输入某年的某月，判断这个月有多少天
  分析：
     1，3，5，7，8，10，12 ： 31天
     4 6 9 11: 30天
     2：闰年：29天  平年：28天
     核心：判断输入的年份是闰年还是平年
      情况1: 年份%400 == 0 闰年   2000年
      情况2：年份%4 ==0 And 年份%100 ！= 0  1900年不是闰年 1900%4==0 1900%100 == 0
"""
""" 方法01：常规写法"""
"""
if __name__ == '__main__':
    # 输入年份转为整数
    year = int(input("请输入年份："))
    # 输入月份转为整数
    month = int(input("请输入月份："))
    # 定义一个天数
    days = 0
    # 判断
    if month in [1, 3, 5, 7, 8, 10, 12]:
        days = 31
    elif month in [4, 6, 9, 11]:
        days = 30
    else:
        # 处理2月份
        if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
            days = 29
        else:
            days = 28

    # 输出
    print("%d年%d月：%d天" % (year, month, days))

"""

# 方法01：巧妙写法

if __name__ == '__main__':
    # 输入年份转为整数
    year = int(input("请输入年份："))
    # 输入月份转为整数
    month = int(input("请输入月份："))
    # 定义一个list
    #            1    2  3   4    5  6    7  8   9   10  11  12
    days_list = [31, 28, 31, 30, 31, 30 , 31, 31, 30, 31, 30, 31]
    #   index     0   1   2   3    4   5   6   7  8    9    10  11

    # 判断是闰年吗
    if (month == 2) and (year % 400 == 0 or (year % 4 == 0 and year % 100 != 0)):
            print("%d年%d月：%d天" % (year, month, days_list[month-1] + 1))
    else:
        print("%d年%d月：%d天" % (year, month, days_list[month - 1]))