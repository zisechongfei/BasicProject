# for --- in range---

if __name__ == '__main__':
    # 从1 加到100之和
    # 定义一个sum记录和
    sum = 0
    # 开始循环
    for i in range(1, 101, 1):
        # range(1, 101, 1)  (起始值， 结束值， 步长)
        # 1, 2, ... 101   i>=起始值，i<结束值
        # [1,2,3,4,5,6,....100]
        # 如果起始值是1，可以省略，如果步长是1，也可以省略
        # for i in range(1, 101, 1):  等价于：for i in range(100)
        sum += i
    # 打印
    print("和为：", sum )
