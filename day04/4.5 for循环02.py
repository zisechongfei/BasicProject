#  计算任何100，500个数间所有5的倍数之和
"""
if __name__ == '__main__':
    # 定义一个变量
    sum = 0
    # 开始循环
    for i in range(100, 501, 5):
        sum += i

    # 输出
    print(sum)
"""

# 练手：求三位数中最大的57的倍数
if __name__ == '__main__':
    # 开始循环
    for i in range(999, 100, -1):
        if i % 57 == 0:
            print(i)
            break
