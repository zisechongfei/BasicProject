"""
Package(包)
Module(模块)
Class(类)
Function(函数)
"""

# 定义一个全局变量
PI = 3.1415926


# 输入圆的半径，计算周长和面积 --- 面向对象的变成（Class / Object ）
# 类的命名的时候：CAMEL --> 首字母大写
class Round:
    # 默认的函数__init__, 构造函数，在实例化对象的时候自动执行
    def __init__(self, radius: float):
        self.radius = radius  # 在创建对象的时候，把初始化的值赋给类的静态成员
        self.perimeter = 0.0
        self.area = 0.0

        # 自动计算周长和面积
        self.get_perimeter()
        self.get_ares()

    def get_perimeter(self):
        # 计算周长
        self.perimeter = 2 * PI * self.radius

    def get_ares(self):
        # 计算面积
        self.area = PI * self.radius * self.radius


import random


# 可以获取一定量的随机的姓名和数字
class RandomItem:
    # 构造函数
    def __init__(self, start, end, number):
        self.start = start
        self.end = end
        self.number = number
        # 定义一个集合
        self.numbers = []
        # 自动执行
        self.get_random_numbers()

    def get_random_numbers(self):
        # 开始循环
        while True:
            # 生成一个随机值
            temp = random.randint(self.start, self.end)
            # 判断是否存在
            if temp not in self.numbers:
                # 附加到集合
                self.numbers.append(temp)
                # 判断数量是否满足
                if len(self.numbers) == self.number:
                    break
