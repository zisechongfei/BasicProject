from django.shortcuts import render, HttpResponse
# 引入models类
from student.models import Student
# 引入Jsonresponse
from django.http import JsonResponse

# Create your views here.

def add_student(request):
    """添加一个学生"""
    # 实例化一个学生
    obj_student = Student(sno='95002', sname="陈鹏", gender="女", birthday="1990-09-10",
                          mobile="13966778899", email="chenpeng@abc.com", address="上海市闵行区银都路111号")
    # 添加到数据库
    obj_student.save()
    # 返回给用户信息
    return HttpResponse("用户添加成功！")


def update_student(request):
    """修改学生信息"""
    # 获取要修改的学生对象 
    obj_student = Student.objects.get(sname="陈鹏")
    # 开始修改 
    obj_student.gender = "女"
    obj_student.address = "上海市山跑路888号"
    # 保存
    obj_student.save()
    return HttpResponse("用户修改成功！")


def delete_student(request):
    """删除某一个学生"""
    # 获取要修改的学生对象 
    obj_student = Student.objects.get(sname="陈鹏")
    # 删除
    obj_student.delete()
    # 保存
    obj_student.save()
    return HttpResponse("用户删除成功！")


def get_all_students(request):
    """获取所有学生信息"""
    # 获取所有学生信息 
    obj_students = list(Student.objects.all().values())

    # 返回信息
    return JsonResponse({'students': obj_students},json_dumps_params={'ensure_ascii':False})