from django.db import models


# Create your models here.
# 当前的models中写的类就是关联到数据库中的表！！！！

# Student的类-- 命名规则：app_class
class Student(models.Model):
    sno = models.CharField(max_length=100, primary_key=True)  # 学号，字符类型，主键
    sname = models.CharField(max_length=100, null=False)  # 姓名，字符类型，不能为空 
    gender = models.CharField(max_length=100)  # 性别，字符类型 
    birthday = models.DateField()  # 出生日期，日期类型
    mobile = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    address = models.CharField(max_length=100)

    def __str__(self):
        return "学号：%s \t 姓名：%s" % (self.sno, self.sname)

