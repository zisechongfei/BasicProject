"""
一副扑克牌52张（除了大小王）, 4个玩家在玩，模拟系统发牌、洗牌和整理牌，具体需求如下：
【1】先按照顺序打印出一副扑克牌
【2】在没有洗牌的情况下，输出发到四个玩家的扑克牌
【3】实现对扑克牌的洗牌，然后输出发到四个玩家的扑克牌
【4】对于洗牌后的四个玩家的扑克牌进行整理
 整理规则1：数字从小到大
	            从小到大(3、4、5、6、7、8、9、10、J、Q、K、A、2)
 整理规则2：在数字相同的情况下，按照花色（黑、红、梅、方）的顺序
"""
import time
import random

# 扑克牌的基础数据
POKER_DICT = {
    'number': ('3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A', '2'),
    #  0    1    2    3    4    5    6     7    8    9    10   11   12
    'type': ("♠", "♥", "♦", "♣")
    #  0   1    2    3

}


# 比如： 黑桃10  --》 10 00    方块6    0602


class PokerGame:
    # 构造函数
    def __init__(self, poker_number: int = 1, person_number: int = 4):
        self.poker_number = poker_number  # 几付扑克牌
        self.person_number = person_number  # 玩家数量
        # 定义变量存储数据
        self.poker_list = []
        # 定义集合存储玩家的牌
        self.person_poker_list = []  # [[第一个玩家],[2],[3],[4]]
        # 定义集合存储整理完玩家的牌
        # self.person_sorted_poker_list = [] # 整理完的4个玩家的牌

    def start(self):
        print("正在启动游戏.....")
        time.sleep(2)  # 让程序暂停2秒
        print("游戏已启动")
        while True:
            print("\n" + "=" * 40 + "扑克牌游戏" + "=" * 40)
            input_number = input("请选择要执行的操作【1-生成牌 2-打印所有牌 3-打印玩家牌 4-发牌 5-洗牌 6-整理牌 7-退出】:")
            # 根据输入的数字，来进行条件选择
            if input_number == "1":
                # 生成牌
                print("正在生成扑克牌.....")
                time.sleep(2)  # 让程序暂停2秒
                self.build()
                print("扑克牌生成完成！")
            elif input_number == "2":
                # 打印所有的牌
                print("正在打印所有的扑克牌.....")
                self.print_all_poker(self.poker_list)
            elif input_number == "3":
                # 打印玩家的牌
                print("正在打印玩家的扑克牌.....")
                self.print_person_poker()
            elif input_number == "4":
                # 发牌
                print("正在发牌.....")
                self.deal_poker()
                print("发牌完毕.....")
            elif input_number == "5":
                # 洗牌
                print("正在洗牌.....")
                random.shuffle(self.poker_list)  # 把list集合打乱
                print("洗牌完毕.....")

            elif input_number == "6":
                # 整理牌
                print("正在整理玩家的牌.....")
                self.person_sorted_poker()
                print("玩家的牌整理完毕.....")

            elif input_number == "7":
                # 退出
                print("游戏已退出，再见！")
                break
            else:
                print("输入的数字不符合要求，请重新输入！")

    def build(self):
        """生成扑克牌"""
        # 定义一个Poker数字
        poker_number = []
        # 生成
        for number in range(13):
            poker_number.append("%02d" % number)
        # 定义Poker花色
        poker_type = ["00", "01", "02", "03"]
        # 生成牌  几付牌--》数字--》花色
        for pair in range(self.poker_number):
            for number in poker_number:
                for type in poker_type:
                    self.poker_list.append(number + type)

    def print_all_poker(self, print_list: list):
        """打印所有的牌"""
        for one_poker in print_list:
            # 0601 --> 红桃9
            print("%s%s" % (POKER_DICT['type'][int(one_poker[2:])], POKER_DICT['number'][int(one_poker[:2])]), end="\t")

    def deal_poker(self):
        """发牌"""
        for person_index in range(self.person_number):
            # 定义一个玩家的临时的集合 第一次：0  第二次：1  第三次：2， 第四次：3
            temp_list = []
            # 遍历牌
            for index in range(len(self.poker_list)):
                # 根据索引判断
                if index % self.person_number == person_index:  # 第一玩家：0  4   8  12   16
                    # 附加到这个玩家的集合                            # 第二玩家：1  5  9  13   17
                    temp_list.append(self.poker_list[index])  # 第三玩家: 2  6  10  14  18
                    # 第四玩家：3   7  11  15  19
            # 把这个玩家的牌添加到集合
            self.person_poker_list.append(temp_list)

    def print_person_poker(self):
        """打印玩家的扑克"""
        for index, value in enumerate(self.person_poker_list):
            # 换行：
            print()
            # 打印提示
            print("第%d个玩家的牌：" % (index + 1), end="")
            self.print_all_poker(self.person_poker_list[index])

    def person_sorted_poker(self):
        """整理玩家手上的牌"""
        # 遍历
        for one_person_poker in self.person_poker_list:
            # 附加到整理好的牌中
            one_person_poker.sort()  # 改变原来的集合

# 知识点：条件选择 ，循环,  List,  Dict ,  面向对象的思维
