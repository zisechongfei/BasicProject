"""
本模块实现和数据库的交互：增删改查
1.。。
2.。。
"""

# === 引入模块 ====
# 导入连接MySQL模块
import pymysql


class MysqlConn:
    # 构造函数
    def __init__(self, conn_infos: dict):
        # 接收初始化的数据
        self.conn_infos = conn_infos # 主机名、用户名、密码、数据库
        # 准备一个集合存储最后的结果
        self.result = []
        # 需要一个变量高速程序是否执行成功！
        self.flag = 0  # 如果等于0：未执行成功 等于1：执行成功
        # 需要一个变量存储异常信息
        self.msg = ""

    def get_db_data(self, sql: str):
        """
        使用SQL语句获取相关表中的数据
        :param sql: 提供的SQL语句
        :return: 返回的结果
        """
        try:
            # 1. 实例化数据库的连接
            mysql_conn = pymysql.connect(self.conn_infos["HOST"], self.conn_infos["USER"],
                                         self.conn_infos["PASSWORD"], self.conn_infos["NAME"])
            # 2. 实例化一个指针
            cursor = mysql_conn.cursor()
            # 3. 使用SQL语句执行
            cursor.execute(sql)
            # 4.获取执行的数据
            self.result = list(cursor.fetchall())   # 返回的结果为（（）,(),(),）-- Tuple的嵌套
            # 5.把Flag改为1
            self.flag = 1
            # 6.关闭数据库
            mysql_conn.close()

        except Exception as e:
            # 如果出现异常，flag = 0 , 捕获系统给的错误
            self.flag = 0
            self.msg = "连接数据库操作出现异常，具体原因：" + str(e)

    def update_db(self, sql: str):
        """
        根据SQL语句修改数据库：包含插入，修改，删除
        :param sql: 提供的SQL语句
        :return:
        """
        # 需要顶一个mysql_conn
        mysql_conn = ""
        try:
            # 1. 实例化数据库的连接
            mysql_conn = pymysql.connect(self.conn_infos["HOST"], self.conn_infos["USER"],
                                         self.conn_infos["PASSWORD"], self.conn_infos["NAME"])
            # 2. 实例化一个指针
            cursor = mysql_conn.cursor()
            # 3. 使用SQL语句执行
            cursor.execute(sql)
            # 4. 提交到数据库去更改
            mysql_conn.commit()
            # 5.把Flag改为1
            self.flag = 1
            # 6.关闭数据库
            mysql_conn.close()

        except Exception as e:
            # 如果出现异常，flag = 0 , 捕获系统给的错误 , 回滚数据库
            self.flag = 0
            self.msg = "连接数据库操作出现异常，具体原因：" + str(e)
            mysql_conn.rollback()


"""
1. 需要提供哪些数据：1） 连接数据库的信息  2）SQL语句
2. 对数据库的操作：增删改查
    1） 第一类：获取数据（查：Select）
    2） 第二该：修改数据库（增Insert 删 Delete 改Update）
3. python中异常处理的结构：
    try: 有可能出现异常的代码
    except: 如果出现异常，执行的代码
    finally: 无论是否有异常都执行的代码，一般用于文件的关闭，数据库关闭 
"""


class MssqlConn:
    pass


class OracleConn:
    pass


class MongoDBConn:
    pass