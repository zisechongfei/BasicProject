# 随机生成10个 50-100之间的整数 -- 随机数
# random.randint(50,100)， 包含两头

import random


def get_random_numbers(start, end, number):
    """
    生成一定范围内一定数量的随机数
    :param start: 范围的开始值
    :param end: 范围的结束值
    :param number: 数量
    :return: 集合
    """
    # 定义一个集合存储结果
    numbers = []
    # 开始循环
    for i in range(number):
        # 添加
        numbers.append(random.randint(start, end))
    # 返回
    return numbers


if __name__ == '__main__':
    print(get_random_numbers(50, 100, 20))
