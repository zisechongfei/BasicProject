"""
案例01：生成10个50-100的数字
案例02：生成10个不同的50-100的数字
案例03：班级有50个同学，每个同学有5门课，随机生成50个同学的成绩(成绩介于50-100之间)
   list = [
        [66,77,56,90,87],
        [58,78,91,67,100],
        [78,90,59,87,99],
        .....
   ]
案例04:
有五名学生【张三，李四，王五，赵六，马七】 每个学生有五门科目【语文、数学、英语、物理、化学】，
为这5位同学随机生成5门考试的成绩【成绩介于50-100间】
需求：按照均分的倒序打印出成绩的明细

"""

import random


def get_random_numbers(start, end, number):
    """
    生成一定范围内一定数量的随机数
    :param start: 范围的开始值
    :param end: 范围的结束值
    :param number: 数量
    :return: 集合
    """
    # 定义一个集合存储结果
    numbers = []
    # 开始循环
    while True:
        # 生成一个随机数
        temp = random.randint(start, end)
        # 判断是否存在
        if temp not in numbers:
            # 附加到集合
            numbers.append(temp)
            # 判断有没有10个
            if len(numbers) == number:
                # 返回
                return numbers


def get_class_result(total_number: int):
    """
    生成一定数量的学生成绩
    :param total_number: 学生数量
    :return: 返回的结果
    """
    # 定义一个集合
    results = []
    # 开始循环
    for index in range(total_number):
        results.append(get_random_numbers(50, 100, 5))
    # 返回
    return results


if __name__ == '__main__':
    # 获取50个学生的成绩
    class_two_result = get_class_result(50)
    for result in class_two_result:
        print(result)

"""
while循环和for循环应用场景：
1. while循环适用于对于循环次数不明确，但是知道循环的结束条件 
2. for循环适用于具有明确次数的循环
"""
