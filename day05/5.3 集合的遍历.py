list01 = [10, 23, 36, 47, 50, 60, 69, 87, 90, 123]
# 取第一个元素：list01[0]
# 取第二个元素：list01[1]
# 取第三个元素：list01[2]
# 取第四个元素：list01[3]

# 遍历：把集合的所有的元素按照顺序读取一遍

# 读取出所有的偶数

# list集合的便利常见的有三种方式：
# 第一种：for -- in  --- 遍历取值
for val in list01:
    if val % 2 == 0:
        print(val)
# 好处：取值非常方便，只能读取，不能修改或者删除

# 第二种： for --in range() --- 遍历索引，通过索引取值
for index in range(0, len(list01), 1):
    if list01[index] % 2 == 0:
        print(list01[index])
# 好处：不但能读，而且还能修改，删除  缺点：取值相对来说比较麻烦

# 第三种：把第一种和第二种的优势集为一身
list01 = [10, 23, 36, 47, 50, 60, 69, 87, 90, 123]
# 如果是3的倍数，就给为666
for index, value in enumerate(list01):
    if value % 3 == 0:
        list01[index] = 666