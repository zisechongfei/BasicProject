from django.shortcuts import render

# Create your views here.


def index(request):
    """返回index.html页面"""
    return render(request, 'index.html')
