# list集合 --- 有序 ，可变 --- 用[]
list01 = [11, 22, 33, 44, 55, 66]
list01[3] = 88
print(list01)

# tuple集合 --- 有序，不可变 / 不可修改的list集合 --- 用（）
tuple01 = (11, 22, 33, 44, 55, 66)
print(tuple01[3])


# set集合 --- 无序，不重复  -- 用{}
set01 = {11, 22, 33, 44, 55, 66, 11, 22}
print(set01)
# 所有元素-->hash值 --> hash值排序 ---》每次打印的先后顺序一样

# 需求：生成10个50-100的不重复的随机值
import random

def get_random_numbers(start,end,number):
    # 定义一个容器
    set01 = set()
    # 循环
    while len(set01) < number:
        set01.add(random.randint(start,end))

    # 返回
    return set01


if __name__ == '__main__':
    print(get_random_numbers(50,100,10))



# dict --- > 其实就是一个特殊的set--> 无序的不能重复的(key-value)的集合

dict01 = {}
dict01['sno'] = '95001'
dict01['name'] = '张三'
dict01['age'] = 30
dict01['name'] = '李四'
dict01['number'] = 30
print(dict01)

# dict的值存储非常灵活


