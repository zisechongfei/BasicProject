"""多线程的基本语法 """
import time
# 引入多线程的模块
import threading


def print_hello(i: int):
    print("Hello,World!--- %d" % i)
    time.sleep(3)  # 让程序暂停1秒


def line_run():
    """单线程的运行"""
    for i in range(5):
        print_hello(i)


def async_run():
    for i in range(5):
        # 实例化一个新线程
        th = threading.Thread(target=print_hello, args=[i])
        # 启动
        th.start()


if __name__ == '__main__':
    # 单线程调用
    # line_run()
    # 多线程调用
    async_run()
