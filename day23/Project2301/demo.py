from iLyncSpider_Thread import iLyncSpider
# 引入time
import time

if __name__ == '__main__':

    # 实例化一个对象
    obj = iLyncSpider()
    # 记录开始时间
    start = time.time()
    # 开始爬取
    obj.run()
    # 记录结束时间
    end = time.time()
    # 打印
    print("程序运行时间：%.4f" %(end-start))
